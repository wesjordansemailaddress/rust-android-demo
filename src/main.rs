mod api;
mod storage;

use storage::{SQLiteStorageProvider, IpAddrEntity};

fn log_ip_addr(db: SQLiteStorageProvider) -> usize {
    let ipaddr = api::get_user_ip().unwrap();
    println!("ip_addr = {:?}", ipaddr.origin);

    let changed = db.add_ip_addr(&storage::IpAddrEntity { ip_addr: ipaddr.origin }).unwrap();
    println!("Wrote {} rows to db.", changed);

    let prev_ip_addrs = db.get_ip_addrs().unwrap();
    let num_prev = prev_ip_addrs.len();
    for prev_ip_addr in prev_ip_addrs {
        println!("prev ip_addr = {:?}", prev_ip_addr.ip_addr);
    }

    num_prev
}

fn main() {
    let db = SQLiteStorageProvider::from_file("/data/local/tmp/androidtest.db").unwrap();
    log_ip_addr(db);
}

mod tests {
    use crate::storage::SQLiteStorageProvider;
    use super::log_ip_addr;

    #[test]
    fn should_log_ip() {
        let db = SQLiteStorageProvider::create_in_memory().unwrap();
        let n = log_ip_addr(db);

        assert_eq!(n, 1);
    }
}