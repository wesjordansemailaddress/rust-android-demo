use serde::Deserialize;

#[derive(Deserialize)]
pub struct HttpBinIpAddr {
    pub origin: String
}

pub fn get_user_ip() -> Result<HttpBinIpAddr, ureq::Error> {
    let resp = ureq::get("https://httpbin.org/ip").call();
    let body = resp.into_json_deserialize()?;

    Ok(body)
}