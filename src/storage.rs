use rusqlite::{Connection, params, NO_PARAMS};

pub struct IpAddrEntity {
    pub ip_addr: String
}

pub struct SQLiteStorageProvider {
    conn: Connection
}

impl SQLiteStorageProvider {
    pub fn create_in_memory() -> Result<SQLiteStorageProvider, rusqlite::Error> {
        let conn = Connection::open_in_memory()?;
        let provider = SQLiteStorageProvider {conn};

        provider.create_schema()?;

        Ok(provider)
    }

    pub fn from_file(filepath: &str) -> Result<SQLiteStorageProvider, rusqlite::Error> {
        let conn = Connection::open(filepath)?;
        let provider = SQLiteStorageProvider {conn};

        if !provider.detect_schema()? {
            println!("Creating schema...");
            provider.create_schema()?;
        } else {
            println!("Using existing schema...");
        }

        Ok(provider)
    }

    fn create_schema(&self) -> rusqlite::Result<()> {
        self.conn.execute_batch("
        BEGIN;
        CREATE TABLE ipaddr (
            id       INTEGER PRIMARY KEY,
            ip_addr  TEXT NOT NULL
        );
        COMMIT;
        ")
    }

    fn detect_schema(&self) -> rusqlite::Result<bool> {
        let has_ipaddr: i32 = self.conn.query_row(
            "SELECT count(*) FROM sqlite_master WHERE type='table' AND name='ipaddr';",
            NO_PARAMS,
            |row| row.get(0),
        )?;

        Ok(has_ipaddr > 0)
    }

    pub fn add_ip_addr(&self, ip_addr: &IpAddrEntity) -> rusqlite::Result<usize> {
        self.conn.execute("INSERT INTO ipaddr (ip_addr) VALUES (?1)", params![ip_addr.ip_addr])
    }

    pub fn get_ip_addrs(&self) -> rusqlite::Result<Vec<IpAddrEntity>> {
        let mut stmt = self.conn.prepare("SELECT ip_addr FROM ipaddr")?;

        let rows = stmt.query_map(NO_PARAMS, |row| {
            let ip_addr = row.get(0)?;
            Ok(IpAddrEntity{ ip_addr })
        } )?;

        let mut entities = Vec::new();
        for name_result in rows {
            entities.push(name_result?);
        }

        Ok(entities)
    }
}