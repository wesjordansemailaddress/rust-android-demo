#!/usr/local/bin/fish

set -x ANDROID_HOME /Users/wes/Library/Android/sdk
set -x NDK_HOME $ANDROID_HOME/ndk/21.3.6528147

set -x PATH $PATH "$ANDROID_HOME/platform-tools" "~/.cargo/bin/" "$PWD/.cargo/bin/" "$PWD/../NDK/arm64/bin/"

set -x PKG_CONFIG_ALLOW_CROSS 1
set -x CC_aarch64_linux_android "$PWD/../NDK/arm64/bin/aarch64-linux-android-clang"

function testrelease
    cargo build --release
    adb push target/aarch64-linux-android/release/androidtest /data/local/tmp/androidtest-release
    adb shell su -c /data/local/tmp/androidtest-release
end